<?php
function dosyaIndir($dosyaUrl, $hedefDosya) {
    $indirilenDosya = file_get_contents($dosyaUrl);
    if ($indirilenDosya !== false) {
        $dosyaBoyutu = file_put_contents($hedefDosya, $indirilenDosya);
        if ($dosyaBoyutu !== false) {
            echo "Dosya indirildi.";
        } else {
            echo "Dosya indirme hatası: Hedef dosyaya yazılamadı.";
        }
    } else {
        echo "Dosya indirme hatası: Dosya alınamadı.";
    }
}

// Get parametresini al
$dosyaUrl = $_GET['dosyaUrl'];
$hedefDosya = $_GET['hedefDosya'];

if ($dosyaUrl && $hedefDosya) {
    dosyaIndir($dosyaUrl, $hedefDosya);
} else {
    echo "Lütfen geçerli bir dosya URL'si ve hedef dosya adı sağlayın.";
}
?>